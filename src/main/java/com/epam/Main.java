package com.epam;

import com.epam.controller.Controller;

public class Main {

    public static void main(String[] args) {
        new Controller().execute();
    }
}
