package com.epam.view;

import com.epam.model.Door;

public interface View {

    public void printMenuMain();
    public void printMenuMainArrays();
    public void printMenuMainGameOptions3(boolean live);
    public void printMenuMainGameOptions2();
    public void printMenuMainGame();
    public void printFalseVariable();
    public void printArray(int[] arr);
    public void printChooseArray();
    public void printAllDoors(Door[] doors);
    public void printHowManyDoorsCanKill(int numberOfDoor);
    public void printFillUpOwnMagicalArtifacts();
    public void printFillUpOwnMonster();
}
