package com.epam.view;

import com.epam.model.Door;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleView implements View {

    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    public void printMenuMainGameOptions2() {
        logger.info("1) Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.\n" +
                "2) Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.\n" +
                "3) Вивести номери дверей в тому порядку, в якому слід їх відкривати \n" +
                "герою, щоб залишитися в живих, якщо таке можливо.\n");
    }

    public void printMenuMainGameOptions3(boolean live) {
        logger.info("3) відкривати двері потрібно в такому порядку : \n");
        logger.info("потрібно відкривати двері спочатку від 1 - 5, а потім будь які інші");
        logger.info(live ? "\n Можлива перемога" : "\n Остаточно поразка");
    }

    public void printMenuMain() {
        logger.info("\n1)Array" + "\n" + "2)Game" + "\n");
    }

    public void printMenuMainGame() {
        logger.info("1)Організувати введення інформації про те, " + "\n" +
                "що знаходиться за дверима" + "\n" +
                "2)Aбо заповнити її, використовуючи генератор випадкових чисел" + "\n");
    }

    public void printMenuMainArrays() {
        logger.info("Сформувати третій масив, що складається з тих елементів, які:" + "\n");
        logger.info("1)присутні в обох масивах;" + "\n" + "2)присутні тільки в одному з масивів;" + "\n" +
                "3)видалити в масиві всі числа, які повторюються більше двох разів;" + "\n" +
                "4)знайти в масиві всі серії однакових елементів, які йдуть підряд, " +
                "і видалити з них всі елементи крім одного;");
    }

    public void printFalseVariable() {
        logger.error("Choose true variable, try again");
    }

    public void printArray(int[] arr) {
        logger.info("\n");
        logger.info("Масив : ");
        for (int k : arr) {
            logger.info(k + " ");
        }
        logger.info("\n");
    }

    public void printChooseArray() {
        logger.info("Choose array first or second:");
    }

    public void printAllDoors(Door[] doors) {
        logger.info("1) За дверима знаходяться: \n");
        for (Door door : doors) {
            logger.info(door + "\n");
        }
    }

    public void printHowManyDoorsCanKill(int numberOfDoor) {
        logger.info("2) Двері за якими чекає смерт: \n");
        logger.info(numberOfDoor + "\n");
    }

    public void printFillUpOwnMagicalArtifacts() {
        logger.info("Введіть силу  MagicalArtifacts: \n");
    }

    public void printFillUpOwnMonster() {
        logger.info("Введіть силу  Monster: \n");
    }
}
