package com.epam.controller;

import com.epam.help.Const;
import com.epam.model.ArrayMethods;
import com.epam.model.Door;
import com.epam.model.Hall;
import com.epam.view.ConsoleView;
import java.util.Scanner;

public class Controller {

    ConsoleView consoleView = new ConsoleView();
    Hall hall = new Hall();
    private Scanner scanner = new Scanner(System.in);

    int[] arr1 = {4, 4, 3, 4};
    int[] arr2 = {5, 5, 5, 4};

    public void execute() {
        consoleView.printMenuMain();
        int choose = scanner.nextInt();
        if (choose == Const.CHOOSE_ONE) {
            consoleView.printMenuMainArrays();
            arrayOptions();
        } else if (choose == Const.CHOOSE_TWO) {
            consoleView.printMenuMainGame();
            gameOptions();
        } else {
            consoleView.printFalseVariable();
            execute();
        }
    }

    public void arrayOptions() {
        consoleView.printArray(arr1);
        consoleView.printArray(arr2);
        int chooses = scanner.nextInt();
        if (chooses == Const.CHOOSE_ONE) {
            consoleView.printArray(new ArrayMethods().addTwoArraysInFinalArray(arr1, arr2));
        } else if (chooses == Const.CHOOSE_TWO) {
            consoleView.printChooseArray();
            if (scanner.nextInt() == Const.CHOOSE_ONE) {
                consoleView.printArray(new ArrayMethods().addOneArrayInFinalArray(arr1));
            } else {
                consoleView.printArray(new ArrayMethods().addOneArrayInFinalArray(arr2));
            }
        } else if (chooses == Const.CHOOSE_THREE) {
            consoleView.printChooseArray();
            if (scanner.nextInt() == Const.CHOOSE_ONE) {
                consoleView.printArray(new ArrayMethods().deleteDigitsWithCountMoreTwo(arr1));
            } else {
                consoleView.printArray(new ArrayMethods().deleteDigitsWithCountMoreTwo(arr2));
            }
        } else if (chooses == Const.CHOOSE_FOUR) {
            consoleView.printChooseArray();
            if (scanner.nextInt() == Const.CHOOSE_ONE) {
                consoleView.printArray(new ArrayMethods().deleteAllWhichCopyOneAfterAnother(arr1));
            } else {
                consoleView.printArray(new ArrayMethods().deleteAllWhichCopyOneAfterAnother(arr2));
            }
        } else {
            consoleView.printFalseVariable();
            arrayOptions();
        }
    }

    public void gameDetailOptions(Door[] doors) {
        consoleView.printAllDoors(doors);
        consoleView.printHowManyDoorsCanKill(hall.howManyDoorsCanKillHero(doors));
        consoleView.printMenuMainGameOptions3(hall.theDoorsOpeningProcedure(doors));
    }

    public void gameOptions() {
        int choose = scanner.nextInt();
        if (choose == Const.CHOOSE_ONE) {
            gameDetailOptions(hall.createOwnList());
        } else if (choose == Const.CHOOSE_TWO) {
            consoleView.printMenuMainGameOptions2();
            gameDetailOptions(hall.fillUpRandom());
        } else consoleView.printFalseVariable();
               gameOptions();
    }
}
