package com.epam.model;

public class MagicalArtifact extends Door {

    public MagicalArtifact(int power) {
        super(power);
    }

    @Override
    public String toString() {
        return "MagicalArtifact{" +
                "power=" + power +
                '}';
    }
}
