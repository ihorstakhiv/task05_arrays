package com.epam.model;

import com.epam.help.Const;
import com.epam.view.ConsoleView;
import java.util.Scanner;

public class Hall {

    Door[] doors = new Door[Const.CYCLE_FINISH];

    public Door[] fillUpRandom() {
        for (int i = Const.CYCLE_START; i < Const.CHOOSE_FIVE; i++) {
            int randomNumber = (int) (Math.random() * Const.MAX_HP_MAGIC_ARTIFACT);
            doors[i] = createMagicalArtifact(randomNumber);
        }
        for (int i = Const.CHOOSE_FIVE; i < Const.CYCLE_FINISH; i++) {
            int randomNumber = (int) (Math.random() * Const.MAX_HP_MONSTER);
            doors[i] = createMonster(randomNumber);
        }
        return doors;
    }

    public Door[] createOwnList() {
        Door[] ownDoors = new Door[Const.CYCLE_FINISH];
        int k = Const.CYCLE_START;
        Scanner scanner = new Scanner(System.in);
        while (k < Const.CYCLE_FINISH) {
            new ConsoleView().printFillUpOwnMagicalArtifacts();
            int powerMagicalArtifact = scanner.nextInt();
            ownDoors[k] = createMagicalArtifact(powerMagicalArtifact);
            k++;
            new ConsoleView().printFillUpOwnMonster();
            int powerMonster = scanner.nextInt();
            ownDoors[k] = createMonster(powerMonster);
            k++;
        }
        return ownDoors;
    }

    public MagicalArtifact createMagicalArtifact(int magicalArtifactPower) {
        return new MagicalArtifact(magicalArtifactPower);
    }

    public Monster createMonster(int monsterPower) {
        return new Monster(monsterPower);
    }

    public Door chooseDoor(Door[] doors) {
        int randomNumber = (int) (Math.random() * Const.CYCLE_FINISH);
        if (doors[randomNumber] instanceof MagicalArtifact) {
            doors[randomNumber].plusPower();
            return doors[randomNumber];
        } else
            doors[randomNumber].minusPower();
        return doors[randomNumber];
    }

    public int howManyDoorsCanKillHero(Door[] doors) {
        int doorsWhichCanKill = Const.CYCLE_START;
        for (Door door : doors) {
            if (door instanceof Monster && door.power > Hero.HERO.getHeroPower()) {
                doorsWhichCanKill++;
            }
        }
        return doorsWhichCanKill;
    }

    public boolean theDoorsOpeningProcedure(Door[] doors) {
        int plusPower = Const.CYCLE_START + Hero.HERO.getHeroPower();
        int minusPower = Const.CYCLE_START;
        for (Door door : doors) {
            if (door instanceof MagicalArtifact) {
                plusPower += door.getPower();
            } else minusPower += door.getPower();
        }
        return plusPower > minusPower;
    }
}


