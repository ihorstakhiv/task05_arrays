package com.epam.model;

public abstract class Door {

    public int power;

    public void minusPower() {
        Hero.HERO.setHeroPower(Hero.HERO.getHeroPower() - power);
    }

    public void plusPower() {
        Hero.HERO.setHeroPower(Hero.HERO.getHeroPower() + power);
    }

    public Door(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Door door = (Door) o;
        return power == door.power;
    }

    @Override
    public String toString() {
        return "Door{" +
                "power=" + power +
                '}';
    }
}
