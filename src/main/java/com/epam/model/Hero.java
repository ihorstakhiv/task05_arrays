package com.epam.model;

import com.epam.help.Const;

public class Hero {

    public static Hero HERO = new Hero();
    public static int heroPower;

    private Hero() {
        this.heroPower = Const.HERO_POWER;
    }

    public int getHeroPower() {
        return heroPower;
    }

    public void setHeroPower(int heroPower) {
        this.heroPower = heroPower;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "heroPower=" + heroPower +
                '}';
    }
}
