package com.epam.model;

public class Monster extends Door {

    public Monster(int power) {
        super(power);
    }

    @Override
    public String toString() {
        return "Monster{" +
                "power=" + power +
                '}';
    }
}

