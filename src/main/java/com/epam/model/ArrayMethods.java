package com.epam.model;

import com.epam.help.Const;

public class ArrayMethods {

    public int[] addTwoArraysInFinalArray(int[] arr1, int[] arr2) {
        int k = Const.CYCLE_START;
        int[] arr3 = new int[arr1.length + arr2.length];
        for (int anArr1 : arr1) {
            arr3[k++] = anArr1;
        }
        for (int anArr2 : arr2) {
            arr3[k++] = anArr2;
        }
        return arr3;
    }

    public int[] addOneArrayInFinalArray(int[] arr1) {
        int[] arr3 = arr1;
        return arr3;
    }

    public int[] deleteDigitsWithCountMoreTwo(int[] arrFinal) {
        int count;
        for (int i = Const.CYCLE_START; i < arrFinal.length - Const.CHOOSE_ONE; i++) {
            count = Const.CYCLE_START;
            for (int j = i + Const.CHOOSE_ONE; j < arrFinal.length; j++) {
                if (arrFinal[i] == arrFinal[j]) {
                    count++;
                    if (count >= Const.CHOOSE_TWO) {
                        arrFinal[i]=Const.CYCLE_START;
                        break;
                    }}
            }
        }
      return arrFinal;
    }

    public int[] deleteAllWhichCopyOneAfterAnother(int[] arrFinal) {
        for (int i = Const.CYCLE_START; i < arrFinal.length - Const.CHOOSE_ONE; i++) {
            if (arrFinal[i] == arrFinal[i + Const.CHOOSE_ONE]) {
                arrFinal[i] = Const.CYCLE_START;
                i--;
            }
        }
    return arrFinal;
    }
}

